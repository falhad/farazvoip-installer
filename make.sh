#!/bin/bash

sudo yum update -y
sudo yum install -y git curl zip unzip

echo "nameserver 178.22.122.100" > /etc/resolv.conf

# install java & gradle 
sudo yum -y install epel-release wget unzip
sudo yum -y install java-1.8.0-openjdk-devel
wget https://services.gradle.org/distributions/gradle-3.4.1-bin.zip
sudo mkdir /opt/gradle
sudo unzip -d /opt/gradle gradle-3.4.1-bin.zip
chmod 777 -R /opt/gradle
export PATH=$PATH:/opt/gradle/gradle-3.4.1/bin/

#install docker & docker-compose
sudo yum install -y yum-utils
sudo yum-config-manager  --add-repo  https://download.docker.com/linux/centos/docker-ce.repo
sudo yum -y install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo systemctl restart docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

make sipmanager image
git clone https://gitlab.com/cs.arcxx/farazvoip-push-to-talk-controller.git
chmod 777 -R farazvoip-push-to-talk-controller 
cd farazvoip-push-to-talk-controller 
./gradlew clean
./gradlew bootJar
sudo docker image build --build-arg JAR_FILE=build/libs/sipmanager-*.jar -t sipmanager:0.0.1 .

#make sipmanager panel image
cd ..
git clone https://gitlab.com/cs.arcxx/farazvoip-web-panel.git
cd farazvoip-web-panel 
sudo docker image build  -t sipmanagerpanel:0.0.1 .

sudo docker network create farazvoipnet
sudo docker volume create --name=mongo