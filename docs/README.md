# FarazVoIP

> FarazVoIP Stack Technical documentation.


##### About
This is a technical document for FarazVoIP project. If you have any question or problem using this document you can reach me via:

Farhad Navayazdan\
+98 930 754 0810\
cs.arcxx@gmail.com\
https://falhad.dev



## Hardware Requirements

These are the minimum requirements for the server. Based on the number of users and their usage it may need to be increased.

| HW | Description |
| ----------- | ----------- |
| OS |  Centos 7  Or Issable 4 ISO |
| Ram | Min. 2Gb |
| HDD/SSD | Min. 100Gb |


## Auto Installation

> [This installer project](https://gitlab.com/cs.arcxx/farazvoip-installer) contains auto-install scripts. Simply clone the project and use the prefered script for auto run the stack.

```
git clone https://gitlab.com/cs.arcxx/farazvoip-installer.git
```

### 0. Login in Docker
> Register and login with this command:
```
docker login
```


### 1. Build Docker Images

> If you don't have the `sipmanager-panel` or `sipmanager` docker Images, first execute [make.sh](https://gitlab.com/cs.arcxx/farazvoip-installer/-/blob/master/make.sh) to download and install Java, Gradle, Docker and Docker-compose and build docker images for you.

```
sudo chmod +x make.sh
sudo ./make.sh
```

### 2. Customize ports

> For change and customize ports you can modify `.env.prod` file.

```
API_IP=http://127.0.0.1
API_PORT=8006
SOCKET_PORT=8020
PANEL_PORT=8030
```

### 3. Run docker-compose 


To run images using docker-compose execute this command: 

```
docker-compose --env-file .env.prod up -d
```

## Manuall Installation

### Install Issabel 4 

If you are using Issable 4 ISO or the Sip Server is remote you should skip this section

```
yum update
yum -y install wget
wget -O - http://repo.issabel.org/issabel4-netinstall.sh | bash
```
 


### Install SDKMan

 
Use instruction from [SDKMan! Website](https://sdkman.io/install) or use this commands to install:

```
curl -s https://get.sdkman.io | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
```

### Install Java

``` 
sdk install java 15.0.1-zulu
``` 


### Install Gradle

``` 
sdk install gradle 6.7
``` 



### Install MongoDB

 
Use VPN to remove country restrictions or Set DNS to [Shekan](https://shecan.ir/tutorials/)
```
sudo nano /etc/resolv.conf
```

Modify & save the file based on [Shekan](https://shecan.ir/tutorials/) address:
```
sudo service network restart
```

Then install MongoDB based on the latest version available on [Mongodb Website](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/).
```
sudo systemctl start mongod
```


### Install Nginx

```
sudo yum install epel-release
sudo yum install nginx
```

Change server port (e.g. 8000) [this will be used for access admin panel]: 
```
cd /etc/nginx
sudo nano nginx.conf
```
Modify & save the file.
```
server {
		listen 8000 default_server;
		listen [::]:8000 default_server;
}
location /{
	try_files $uri $uri/ /index.html;
}
```
	 
 
### Install SipManager

 
Copy project to sipmanager dir on VPS or clone it with:

```
git clone https://gitlab.com/cs.arcxx/farazvoip-push-to-talk-controller.git
cd sipmanager
fuser -k 8080/tcp
gradle build
```

Check Jar file Version before run this cmd (e.g. sipmanager-x.y.z.jar):
```
nohup java -jar build/libs/sipmanager-*.jar &
```



### Install Admin Panel

``` 
rm -rf /usr/share/nginx/html/
cd /usr/share/nginx/html/
sudo chown $USER:$USER ../html
```

Copy ‘dist’ folder contents on html folder on VPS or clone it with:
```
git clone https://gitlab.com/cs.arcxx/farazvoip-web-panel.git
```
Restart nginx for changes take effect.
```
sudo systemctl start nginx
sudo service network restart
```

You can modify the Default IP & Port of webservice inside the `‘dist/config.json’` file. For more info see admin panel’s README.md file.



## Routes & Ports
These are default ports that this system works with. If you need to change any of these remember to update Sipmanager & Admin panel too.


| Service | Route | Port | Extra |
| ----------- | ----------- | ----------- |----------- |
|  Admin Panel |{server ip}:8000/|  8000 | admin / faraz123  |
| SipManager RestAPI  | {server ip}:8080/status  |  8080 |  - |
|  SipManager Socket | {server ip}:8010/echo  | 8010  |  - |
|  Issabel  Admin Panel | {Server ip}/  |  80 |  admin / {installation pw} |
|  Sip Protocol |  - | 5060/5061  |  - |


## Issabel 4 Manuall

### Create Conference

Steps to create conference in issable pbx panel

1. Login to panel
2. Nav to PBX
3. Nav to PBX Configurations
4. Nav to Conferences
5. Press Add Conference
6. Set Conference Number and Conference  Name to desire number (e.g. 300)
7. The rest of the options could be No or change it depends on the situation.
8. Submit and Apply Configs to save the conference


### Create Extensions (User)

1. Login to panel
2. Nav to PBX
3. Nav to PBX Configurations
4. Nav to Extensions
5. Press Submit with Generic Sip Device option selected
6. Set User Extension, Display Name, and Sip Alias to desire number 
7. Set Secret to desire user password
8. Check Transport selected  in All - UDP Primarily
9. Press Submit and then Apply Configs


## Admin Panel 

### Create Conference

1. Nav to کنفرانس ها
2. Press افزودن کنفرانس
3. Enter Issabel Server IP and Conference Number
4. Save


### Create User

1. Nav to کاربرها
2. Press افزودن کاربر
3. Enter User Number, User Display Name, User Secret in the fields
4. Save


## T-Shoot

### Conference calls can not be made


1. Login to Issabel panel from web
2. Navigate to System
3. Navigate to Hardware Detector
4. Check Advanced 
5. Check Replace file  chan_dahdi.conf
6. Press Detect new hardware



### yum update not works


> unable to resolve domain after install issabel
```
sudo vi /etc/resolve.conf
Nameserver 8.8.8.8
Nameserver 1.1.1.1
:wq
```

## Restart Server

> In case of server has been shut down or restarted and services didn’t start automatically:

```
cd sipmanager/
nohup java -jar build/libs/sipmanager-*.jar &
sudo systemctl start nginx
```